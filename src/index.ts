import express from 'express';
import mongoose from 'mongoose';
import { json } from 'body-parser';
import { userRouter } from './routes/user';

const app = express()
app.use(json())
app.use(userRouter)

mongoose.connect('mongodb+srv://typeahead:tETFarE84nqwZPTX@typeaheadcluster-lhren.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}, ()=> {
    console.log('connected to database')
})

app.listen(3000, () => {
    console.log('server is listening on port 3000')
})