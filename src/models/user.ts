import mongoose from "mongoose";

interface IUser{
    name:string;
    times:number;
}
const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    times: {
        type: Number,
        required: true
    }
})

const User = mongoose.model('User', userSchema)

export { User, IUser }