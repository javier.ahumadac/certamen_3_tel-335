import express, {Request, Response} from "express";
import { User } from "../models/user";

const router = express.Router()

router.get("/clicks/user/:name", async (request, response) => {
    const user = await User.find({name: { "$regex": request.params.name, "$options": "i" }});
    // para mas informacion en esta funcion ver https://docs.mongodb.com/manual/reference/operator/query/regex/
    try {
        if(user.length == 0){
            response.status(200).send({
                "message": "There was no coincidences"
            });
        }
        else{
            response.send(user);
        }
    } catch (error) {
      response.status(500).send({
          "message":"Internal server error"
      });
    }
  });


export { router as userRouter }